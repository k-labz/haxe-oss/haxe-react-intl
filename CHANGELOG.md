# Changelog

## 0.1.0 (2018-05-01)

* Added ReactIntl API. See [`ReactIntl.hx`](/src/react/intl/ReactIntl.hx).
* Added React Intl formatting components. See [`react.intl.comp`](/src/react/intl/comp/).
* Added [react-router](/examples/react-router/) & [translations](/examples/translations/) example apps.

